package programacionmodular;

import java.util.Scanner;

public class ContarDias {

	private static final boolean True = false;

	static Scanner entrada = new Scanner(System.in);
	public static void main(String[] args) {
		
		/*30 dias: abril, junio, septiembre y noviembre.
		 * 31 dias: enero, marzo, julio, mayo, agosto, octubre, diciembre
		 */

		//Arrancamos la aplicaci�n
		arrancarAplicacion();
	}
		
	private static void arrancarAplicacion() {
		int mes = pedirMes();
		int anno = 0;
		int dia = pedirDia();
		
		boolean fechaValida = false;
		do {
			anno = pedirAnno();
			fechaValida = validarDatos(anno);
		} while(!fechaValida);
		
		switch(mes){
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("mes 30 dias");
			break;
		case 1:
		case 3:
		case 7:
		case 5:
		case 8:
		case 10:
		case 12:
			System.out.println("mes 31 dias");
			break;
		case 2:
			boolean esBisiesto = false;
			esBisiesto = esBisiesto(anno);
			if(esBisiesto) {
				System.out.println("el mes tiene 29 d�as");
			}else {
				System.out.println("el mes tiene 28 d�as");
			}			
		}
	}
	
	/**
	 * este m�todo comprueba si el a�o es bisiesto
	 * @param anno
	 * @return boolean 
	 */
	private static boolean esBisiesto(int anno) {
		boolean esAnnoBisiesto = false;
		if((anno %4 ==0)&&(anno %100!=0)||(anno %400==0)) {
			esAnnoBisiesto = true;
		}
		return esAnnoBisiesto;
	}

	/**
	 * este m�todo comprueba si el a�o introducido es v�lido
	 * @param anno
	 * @return
	 */
	private static boolean validarDatos(int anno) {
		if(anno > 1582) {
			return true;
		}
		return false;
	}

	/**
	 * este m�todo pide un d�a y lo devuelve
	 * @return
	 */
	private static int pedirDia() {
		System.out.println("Introduzca un dia");
		int dia = entrada.nextInt();
		return dia;
	}

	/**
	 * este m�todo pide un a�o y lo devuelve
	 * @return
	 */
	private static int pedirAnno() {
		System.out.println("Introduzca un anno");
		int anno = entrada.nextInt();
		return anno;
	}
	
	/**
	 * este m�todo pide un mes y lo devuelve
	 * @return
	 */
	private static int pedirMes() {
		System.out.println("Introduzca un mes");
		int mes = entrada.nextInt();
		return mes;
	}

}
