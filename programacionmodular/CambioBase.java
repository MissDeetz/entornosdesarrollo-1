package programacionmodular;

import java.util.Scanner;

public class CambioBase {
	
	static Scanner entrada = new Scanner(System.in);	

	public static void main(String[] args) {
		
		int numero = pedirNum();
		int base = pedirBase();
		int cambioBase = cambiarBase(base, numero);
	}

	/**
	 * pide un n�mero y lo devuelve
	 * @return int
	 */
	private static int pedirNum() {
		System.out.println("Introduzca un n�mero");
		int num = entrada.nextInt();
		return num;
	}

	private static int cambiarBase(int base, int numero) {
		if(base <= 10) {
			calcularBase(numero, base);
		}else{

			}
		return 0;
	}

	private static void calcularBase(int numero, int base) {
		if(numero < base) {
			System.out.print(numero);
		}else{
			calcularBase(numero/base, base);
			System.out.print(numero%base);
		}
		
	}

	/**
	 * pide una base y la devuelve
	 * @return int
	 */
	private static int pedirBase() {
		System.out.println("Introduzca una base: ");
		int base = entrada.nextInt();
		return base;
	}
}