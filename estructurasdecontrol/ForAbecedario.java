package estructurasdecontrol;

public class ForAbecedario {

	public static void main(String[] args) {
		
		//int tamAbc = 'Z' - 'A' +1;
		for (char letra = 'a'; letra <='z'; letra++) {
			System.out.print(letra+" ");
		}
		System.out.print("\n\n");
		
		for (int i='A'; i <='Z'; i++) {
			System.out.print(i+" ");
		}
	}

}
 