package estructurasdecontrol;

import java.util.Scanner;

/*este programa hace 
 * un tri�ngulo pitag�rico.
 */
public class TrianguloPitagorico {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int numero;
		System.out.println("Introduzca el n�mero de filas");
		numero = entrada.nextInt();
		
		for(int i = 1; i <= numero; i++ ) {
			for(int j = 1; j <= i; j++ ) {
				System.out.print("*");
			}
			System.out.print("\n");	
		}
		
	}

}
