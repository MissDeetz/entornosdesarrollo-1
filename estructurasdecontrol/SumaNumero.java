package estructurasdecontrol;
import java.io.InputStream;
import java.util.Scanner;
public class SumaNumero {

	public SumaNumero(InputStream in) {
	
	}
	//Importamos la clase scanner.
	
	/*Este programa introduce por teclado 2 numeros 
	y visualiza en pantalla su suma*/
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	//creamos un objeto de tipo scanner
	
		Scanner entrada = new Scanner (System.in);
		
		//Introducimos por teclado los numeros 
		
		System.out.println("Introduce el numero1");
		int num1 = entrada.nextInt();
		System.out.println("Introduce el numero2");
		int num2 = entrada.nextInt();
		
	//Calculamos la suma
		
		int suma = num1 + num2;
		
	//Enviamos a pantalla el resultado.
		
		System.out.println("la suma es " +suma);
	}

}
