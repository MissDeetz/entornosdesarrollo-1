package estructurasdecontrol;

import java.util.Scanner;

public class RepetirPalabra {

	public static void main(String[] args) {
		
		/*Este programa muestra por
		 * pantalla la misma palabra 5 veces.
		 */

		
		System.out.println("Introduzca una palabra");
		Scanner entrada = new Scanner(System.in);
		String nombre = entrada.next();
		for(int i = 1; i <= 5; i++) {
			System.out.println(i);
			System.out.println("La palabra es " +nombre);
		}
		
		for(int i = 5; i >=1; i--) {
			System.out.println(i);
			System.out.println("La palabra es " +nombre);
		}
		int veces = 5;
		while(veces > 1) {
			veces = veces-1;
			System.out.println("la palabra es " +nombre);
		}
	}

}
